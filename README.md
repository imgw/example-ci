# Example-CI

Example of Continuous Integration in Gitlab + Gitlab Runner @ IMGW

## Participation & Support

### Everybody reporting issues

Please write an email to the [service desk](mailto:flexpart-example-issue-@univie.ac.at) and an issue will be created. This does not require an account on GitLab!

### Gitlab users

Please login and open an issue.

## Fortran

We add a src directory with some fortran netcdf code inside from [ucar unidata](https://www.unidata.ucar.edu/software/netcdf/examples/programs/).

files:
 - src/Makefile
 - src/main.f90

## Makefile

In order to build the application we need to define a Makefile with some special things:

```makefile
# use the environmental variable $INCLUDE
# split the paths separated by :
INC = $(subst :, ,$(INCLUDE))
# add a -I/path/to/include
INC := $(INC:%=-I%)
# use the environmental variable $LIBRARY_PATH
LIBS = $(subst :, ,$(LIBRARY_PATH))
LIBS := $(LIBS:%=-L%)
```
that allow to read environmental flags for include and library path. the next part is just to compile all sources to objects and link the application:

```makefile
# build main from main.o using netcdf library
main: main.o
	${FC} $< $(INC) $(LIBS) -lnetcdff -o $@
	
# build source files to objects with include paths
%.o %.mod %.smod: %.f90
	${FC} ${INC} -c -o $*.o $<
```

## Gitlab-CI

The gitlab-ci (continous integration) allows to define some jobs. Every jobs runs on its own, but you can use artifacts or cache to transfer files between jobs.

We can setup a default image to start off and install some necessary libraries for building the application.

```yaml
image: ubuntu:18.04

stages:
  - build

build-ubuntu:
  stage: build

  before_script:
    - apt-get update -qq && apt-get install -y -qq gfortran libnetcdf-dev libnetcdff-dev libeccodes-dev netcdf-bin

  script:
    - export FC=gfortran
    - export LIBRARY_PATH=/usr/local/lib/x86_64-linux-gnu:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu
    - export INCLUDE=/usr/include
    - make -C src
  
  artifacts:
    paths:
      - ./src/main
```

We use the package manager and install the libraries, which you need to find out what names they have, use a site like [pkgs.org](https://pkgs.org) to find out what libraries contains whcih files and versions, so that you can build your container as you need.

Here we use the an LTS ubuntu container with some older version that are provides via the package manager. At a later stage we can build images that have the exact same versions as on some HPC centers.

We need to give some relevant paths to the MAkefile in order to link the netcdf library to our application.

Then we can run our first pipeline and see if everything works.
